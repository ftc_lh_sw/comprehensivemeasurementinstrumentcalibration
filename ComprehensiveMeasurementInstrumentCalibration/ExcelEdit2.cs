﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Web;
using System.Reflection; // 引用这个才能使用Missing字段 
using Microsoft.Office.Core;
using Microsoft.Office.Interop;
using System.Windows.Forms;


namespace ComprehensiveMeasurementInstrumentCalibration
{
    class ExcelEdit2
    {
        object Nothing = System.Reflection.Missing.Value;     
        public Microsoft.Office.Interop.Excel.Workbook workBook;       //工作簿
        public Microsoft.Office.Interop.Excel.Worksheet worksheet;      //工作表

        public string fileName;                                         //文件名
        public string filePath;                                        //文件路徑
        public bool ExcelClose = false;         //excel打開？ = 否
        Microsoft.Office.Interop.Excel.Application excelApp = new Microsoft.Office.Interop.Excel.Application();   //新建一個 excel操作


        public void CreateWiFiExcel() //創建一個工作簿
        {
            fileName = DateTime.Now.ToString("yyyy-MM-dd-HH-mm-ss");        // 文件名字 == 年月日時分秒
            filePath = AppDomain.CurrentDomain.BaseDirectory;               //獲取.exe程序所在的根目錄   
            workBook = excelApp.Workbooks.Add(Nothing);
            worksheet = (Microsoft.Office.Interop.Excel.Worksheet)workBook.Sheets[1];
            //Microsoft.Office.Interop.Excel.Range allRang = worksheet.Columns;
            worksheet.Name = fileName;
            workBook.Sheets[2].Delete();//刪除多餘工作表
            workBook.Sheets[2].Delete();
            excelApp.Visible = false;

            //初始化工作表
            worksheet.Cells[1, 1] = "LTE对WiFi影响的实验";
            worksheet.Cells[2, 1] = "LTE状态：";
            worksheet.Cells[3, 1] = "CMW500LossOut(dB):";
            worksheet.Cells[4, 1] = "CMW500LossIn(dB):";
            worksheet.Cells[5, 1] = "CMW500Band:";
            worksheet.Cells[6, 1] = "CMW500ChannelUl:";
            worksheet.Cells[7, 1] = "CWM270LossOut(dB):";
            worksheet.Cells[8, 1] = "CMW270LossIn(dB):";
            worksheet.Cells[9, 1] = "CMW270Standard:";
            worksheet.Cells[10, 1] = "CMW270Channel:";
            worksheet.Cells[11, 1] = "CMW270TXBurstPower(dBm):";
            CellEdit(); //單元編輯
            VariablesParameter.FileExit = true;
        }


        public void CreateLTEExcel() //創建一個工作簿
        {
            fileName = DateTime.Now.ToString("yyyy-MM-dd-HH-mm-ss");        // 文件名字 == 年月日時分秒
            filePath = AppDomain.CurrentDomain.BaseDirectory;               //獲取.exe程序所在的根目錄   
            workBook = excelApp.Workbooks.Add(Nothing);
            worksheet = (Microsoft.Office.Interop.Excel.Worksheet)workBook.Sheets[1];
            //Microsoft.Office.Interop.Excel.Range allRang = worksheet.Columns;
            worksheet.Name = fileName;
            workBook.Sheets[2].Delete();//刪除多餘工作表
            workBook.Sheets[2].Delete();
            excelApp.Visible = false;    //可见的表格

            //初始化工作表
            worksheet.Cells[1, 1] = "WiFi对LTE影响的实验";
            worksheet.Cells[2, 1] = "WiFi状态：";
            worksheet.Cells[3, 1] = "CMW500LossOut(dB):";
            worksheet.Cells[4, 1] = "CMW500LossIn(dB):";
            worksheet.Cells[5, 1] = "CMW500Band:";
            worksheet.Cells[6, 1] = "CMW500ChannelUl:";
            worksheet.Cells[7, 1] = "CWM270LossOut(dB):";
            worksheet.Cells[8, 1] = "CMW270LossIn(dB):";
            worksheet.Cells[9, 1] = "CMW270Standard:";
            worksheet.Cells[10, 1] = "CMW270Channel:";
            worksheet.Cells[11, 1] = "CMW500RSEPRE(dBm):";
            worksheet.Cells[12, 1] = "Full Cell BW power(dBm):";
            CellEdit(); //單元編輯
            VariablesParameter.FileExit = true;
        }

        public void CellEdit()  //單元編輯
        { 
            Microsoft.Office.Interop.Excel.Range allRang = worksheet.Columns;
            allRang.AutoFit();                       //單元格自適應內容大小
            worksheet.Rows[11].Select();
            excelApp.ActiveWindow.FreezePanes = true;//凍結窗口
            worksheet.EnableSelection = Microsoft.Office.Interop.Excel.XlEnableSelection.xlNoSelection;//不可選擇單元格
            worksheet.Protect(Contents: true, UserInterfaceOnly: true);//設置工作表不可編輯

            //Microsoft.Office.Interop.Excel.Range rng_A1_I1 = worksheet.Range["A1:I1"];//選擇單元格
            //rng_A1_I1.Borders.LineStyle = Microsoft.Office.Interop.Excel.XlLineStyle.xlContinuous;//為單元格加入線框設置線框的格式
            //rng_A2_F2.Borders.Weight = Microsoft.Office.Interop.Excel.XlBorderWeight.xlThin;//設置邊框的寬度
            //rng_A1_I1.Interior.Color = System.Drawing.ColorTranslator.ToOle(System.Drawing.Color.PowderBlue);//設置單元格的背景顏色

            //Microsoft.Office.Interop.Excel.Range rng_A2_I2 = worksheet.Range["A2:I2"];
            //rng_A2_I2.Borders.LineStyle = Microsoft.Office.Interop.Excel.XlLineStyle.xlContinuous;
            //rng_A2_I2.Interior.Color = System.Drawing.ColorTranslator.ToOle(System.Drawing.Color.PowderBlue);

            //Microsoft.Office.Interop.Excel.Range rng_A3_F3 = worksheet.Range["A3:F3"];
            //rng_A3_F3.Borders.LineStyle = Microsoft.Office.Interop.Excel.XlLineStyle.xlContinuous;
            //rng_A3_F3.Interior.Color = System.Drawing.ColorTranslator.ToOle(System.Drawing.Color.LightCyan);

            //Microsoft.Office.Interop.Excel.Range rng_A4_F4 = worksheet.Range["A4:F4"];
            //rng_A4_F4.Borders.LineStyle = Microsoft.Office.Interop.Excel.XlLineStyle.xlContinuous;
            //rng_A4_F4.Interior.Color = System.Drawing.ColorTranslator.ToOle(System.Drawing.Color.LightSkyBlue);
        }

        //講儀器的輸出值寫入到工作表
        //public void WriteToExcel(ACPowerAnalyzerControlPanel f)//在這個類中修改WINFORM控件
        public void WriteToExcel(int row,int column,string strValue) //bool 是复选框滚动  
        {
            try
            {
                worksheet.Cells[row, column] = strValue;
            }
            catch (Exception)
            {
                ExcelClose = true;
                //f.WinFormControl_End();//調用winform裏面的方法
            }
        }


        //將測試所用總時間寫入到相應欄位
        //public void UpdateCumulativeTime()
        //{
        //    worksheet.Cells[3, 6] = VariablesParameter.CumulativeTime;
        //}

        //保存并關閉工作簿
        public void SaveAndClose()
        {
            //string Version;
            //Version = Excel.Version;//獲取已安裝office版本
            //if (double.Parse(Version) < 12)//Excel版本 97-2003
            //   
            //else//Excel版本 2007及以上
            // 
            //UpdateCumulativeTime();  //將測試所用總時間寫入到相應欄位
            try
            {
                worksheet.EnableSelection = Microsoft.Office.Interop.Excel.XlEnableSelection.xlNoRestrictions;  //啟用選擇
            }
            catch (Exception)
            {
                MessageBox.Show("表格未写入数据！");
            }
            //worksheet.EnableSelection = Microsoft.Office.Interop.Excel.XlEnableSelection.xlNoRestrictions;  //啟用選擇
            worksheet.Protect(Contents: false, UserInterfaceOnly: false);                                   //工作表恢復可編輯的狀態
            workBook.SaveAs(filePath + fileName, System.Type.Missing, System.Type.Missing, System.Type.Missing, System.Type.Missing, System.Type.Missing, Microsoft.Office.Interop.Excel.XlSaveAsAccessMode.xlNoChange, System.Type.Missing, System.Type.Missing, System.Type.Missing, System.Type.Missing, System.Type.Missing);
            workBook.Close(false, Type.Missing, Type.Missing);
            excelApp.Quit();
        }
    }
}


