﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace ComprehensiveMeasurementInstrumentCalibration
{
    public partial class Form1 : Form
    {
       VariablesParameter Parameter = new VariablesParameter();        // 新建 變量參數
        InstrumentControl instrumentCMW500 = new InstrumentControl();   //新建 內部儀器控制
        InstrumentControl instrumentN9030A = new InstrumentControl();   //新建 內部儀器控制
        InstrumentControl instrument53230A = new InstrumentControl();   //新建 內部儀器控制

        public Form1()
        {
            InitializeComponent();
        }

        private void button_InstrumentIDN_CMW500_Click(object sender, EventArgs e)
        {
            if (checkIsCMW500Connected() == false) return;
            //initCMW500();  //初始化 lossin lossout band FDD/TDD
            //textBox_CMW500_ChannelUl.Text = get500ChannelUL();  
        }

        private void button_InstrumentIDN_N9030A_Click(object sender, EventArgs e)
        {
            if (checkIsN9030AConnected() == false) return;
        }

        private void button_InstrumentIDN_53230A_Click(object sender, EventArgs e)
        {
            if (checkIs53230AConnected() == false) return;
        }

        /// <summary>
        /// 判断CMW500是否连接
        /// </summary>
        /// <returns></returns>
        private bool checkIsCMW500Connected()
        {
            bool isConnected = false;
            getInstrumentIDNCMW500();
            if (VariablesParameter.InstrumentConnected500 == true)
            {
                isConnected = true;
            }
            else
            {
                MessageBox.Show("連接儀器CMW500失敗! 請確保儀器已經打開！");
            }
            return isConnected;
        }


        private bool checkIsN9030AConnected()
        {
            bool isConnected = false;
            getInstrumentIDNN9030A();
            if (VariablesParameter.InstrumentConnectedN9030A == true)
            {
                isConnected = true;
            }
            else
            {
                MessageBox.Show("連接儀器N9030A失敗! 請確保儀器已經打開！");
            }
            return isConnected;
        }


        private bool checkIs53230AConnected()
        {
            bool isConnected = false;
            getInstrumentIDN53230A();
            if (VariablesParameter.InstrumentConnected53230A == true)
            {
                isConnected = true;
            }
            else
            {
                MessageBox.Show("連接儀器53230A失敗! 請確保儀器已經打開！");
            }
            return isConnected;
        }

        /// <summary>
        /// 獲取設備CMW500的信息并顯示
        /// </summary>
        private void getInstrumentIDNCMW500()
        {
            VariablesParameter.InterfaceAndAddressCMW500 = comboBox_Interface_CMW500.SelectedItem + "::" + comboBox_Address_CMW500.SelectedItem;
            instrumentCMW500.InstrumentInitialize(VariablesParameter.InterfaceAndAddressCMW500);
            VariablesParameter.INSTRUMENTID = instrumentCMW500.ReadVISACommandResult("*IDN?");
            if (VariablesParameter.INSTRUMENTID == "")
                textBox_CMW500_Massage.Text = VariablesParameter.INSTRUMENTNOTFOUND;
            else
            {
                textBox_CMW500_Massage.Text = VariablesParameter.InterfaceAndAddressCMW500 + Environment.NewLine + VariablesParameter.INSTRUMENTID;
                VariablesParameter.InstrumentConnected500 = true;
            }
        }


        private void getInstrumentIDNN9030A() //获取N9030A的信息并展示
        {
            VariablesParameter.InterfaceAndAddressN9030A = comboBox_Interface_N9030A.SelectedItem + "::" + comboBox_Address_N9030A.SelectedItem;
            instrumentN9030A.InstrumentInitialize(VariablesParameter.InterfaceAndAddressN9030A);
            VariablesParameter.INSTRUMENTID = instrumentN9030A.ReadVISACommandResult("*IDN?");
            if (VariablesParameter.INSTRUMENTID == "")
                textBox_N9030A_Massage.Text = VariablesParameter.INSTRUMENTNOTFOUND;
            else
            {
                textBox_N9030A_Massage.Text = VariablesParameter.InterfaceAndAddressN9030A + Environment.NewLine + VariablesParameter.INSTRUMENTID;
                VariablesParameter.InstrumentConnectedN9030A = true;
            }
        }


        private void getInstrumentIDN53230A() //获取53230A的信息并展示
        {
            VariablesParameter.InterfaceAndAddress53230A = comboBox_Interface_53230A.SelectedItem + "::" + comboBox_Address_53230A.SelectedItem;
            instrument53230A.InstrumentInitialize(VariablesParameter.InterfaceAndAddress53230A);
            VariablesParameter.INSTRUMENTID = instrument53230A.ReadVISACommandResult("*IDN?");
            if (VariablesParameter.INSTRUMENTID == "")
                textBox_53230A_Massage.Text = VariablesParameter.INSTRUMENTNOTFOUND;
            else
            {
                textBox_53230A_Massage.Text = VariablesParameter.InterfaceAndAddress53230A + Environment.NewLine + VariablesParameter.INSTRUMENTID;
                VariablesParameter.InstrumentConnected53230A = true;
            }
        }
         
        private int getFrequencyArray(TextBox arrayTextBox) 
        {
            int arrayLength = 0;
            
            VariablesParameter.CMW500ChannelArray = (arrayTextBox.Text).Split(new[] { "," }, StringSplitOptions.None);
            arrayLength = VariablesParameter.CMW500ChannelArray.Length;
            for (int i = 0, a = 0; i < arrayLength; i++)
            {
                if (int.TryParse(VariablesParameter.CMW500ChannelArray[i], out a) == false && VariablesParameter.CMW500ChannelArray[i] != "") //判断是否可以转换为整型
                {
                    MessageBox.Show("請以類似 14000,15000,16000 的格式輸入！");
                    return arrayLength;
                }
            }
            return arrayLength;
        }

        private void button_TXfrequencyAccuracy_Click(object sender, EventArgs e)
        {
            if (checkIs53230AConnected() == false) return;
            if (checkIsCMW500Connected() == false) return;

            if (getFrequencyArray() == false) return;

        }









    }
}
