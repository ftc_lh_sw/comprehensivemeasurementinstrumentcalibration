﻿namespace ComprehensiveMeasurementInstrumentCalibration
{
    partial class main
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.button1 = new System.Windows.Forms.Button();
            this.button2 = new System.Windows.Forms.Button();
            this.button3 = new System.Windows.Forms.Button();
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.groupBox2 = new System.Windows.Forms.GroupBox();
            this.button4 = new System.Windows.Forms.Button();
            this.textBox1 = new System.Windows.Forms.TextBox();
            this.label1 = new System.Windows.Forms.Label();
            this.label16 = new System.Windows.Forms.Label();
            this.comboBox_Interface_CMW500 = new System.Windows.Forms.ComboBox();
            this.label7 = new System.Windows.Forms.Label();
            this.textBox5 = new System.Windows.Forms.TextBox();
            this.textBox2 = new System.Windows.Forms.TextBox();
            this.comboBox1 = new System.Windows.Forms.ComboBox();
            this.textBox3 = new System.Windows.Forms.TextBox();
            this.label2 = new System.Windows.Forms.Label();
            this.textBox4 = new System.Windows.Forms.TextBox();
            this.comboBox2 = new System.Windows.Forms.ComboBox();
            this.textBox6 = new System.Windows.Forms.TextBox();
            this.label3 = new System.Windows.Forms.Label();
            this.textBox7 = new System.Windows.Forms.TextBox();
            this.comboBox3 = new System.Windows.Forms.ComboBox();
            this.textBox8 = new System.Windows.Forms.TextBox();
            this.label4 = new System.Windows.Forms.Label();
            this.textBox9 = new System.Windows.Forms.TextBox();
            this.comboBox4 = new System.Windows.Forms.ComboBox();
            this.textBox10 = new System.Windows.Forms.TextBox();
            this.label5 = new System.Windows.Forms.Label();
            this.textBox11 = new System.Windows.Forms.TextBox();
            this.comboBox5 = new System.Windows.Forms.ComboBox();
            this.textBox12 = new System.Windows.Forms.TextBox();
            this.label6 = new System.Windows.Forms.Label();
            this.textBox13 = new System.Windows.Forms.TextBox();
            this.comboBox6 = new System.Windows.Forms.ComboBox();
            this.textBox14 = new System.Windows.Forms.TextBox();
            this.label8 = new System.Windows.Forms.Label();
            this.groupBox1.SuspendLayout();
            this.groupBox2.SuspendLayout();
            this.SuspendLayout();
            // 
            // button1
            // 
            this.button1.Location = new System.Drawing.Point(27, 23);
            this.button1.Name = "button1";
            this.button1.Size = new System.Drawing.Size(75, 23);
            this.button1.TabIndex = 0;
            this.button1.Text = "CMW500";
            this.button1.UseVisualStyleBackColor = true;
            this.button1.Click += new System.EventHandler(this.button1_Click);
            // 
            // button2
            // 
            this.button2.Location = new System.Drawing.Point(27, 52);
            this.button2.Name = "button2";
            this.button2.Size = new System.Drawing.Size(75, 23);
            this.button2.TabIndex = 1;
            this.button2.Text = "CMU200";
            this.button2.UseVisualStyleBackColor = true;
            // 
            // button3
            // 
            this.button3.Location = new System.Drawing.Point(27, 81);
            this.button3.Name = "button3";
            this.button3.Size = new System.Drawing.Size(75, 23);
            this.button3.TabIndex = 2;
            this.button3.Text = "8960";
            this.button3.UseVisualStyleBackColor = true;
            // 
            // groupBox1
            // 
            this.groupBox1.Controls.Add(this.button1);
            this.groupBox1.Controls.Add(this.button3);
            this.groupBox1.Controls.Add(this.button2);
            this.groupBox1.Location = new System.Drawing.Point(12, 12);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Size = new System.Drawing.Size(309, 215);
            this.groupBox1.TabIndex = 3;
            this.groupBox1.TabStop = false;
            this.groupBox1.Text = "设备";
            // 
            // groupBox2
            // 
            this.groupBox2.Controls.Add(this.textBox13);
            this.groupBox2.Controls.Add(this.comboBox6);
            this.groupBox2.Controls.Add(this.textBox14);
            this.groupBox2.Controls.Add(this.label8);
            this.groupBox2.Controls.Add(this.textBox11);
            this.groupBox2.Controls.Add(this.comboBox5);
            this.groupBox2.Controls.Add(this.textBox12);
            this.groupBox2.Controls.Add(this.label6);
            this.groupBox2.Controls.Add(this.textBox9);
            this.groupBox2.Controls.Add(this.comboBox4);
            this.groupBox2.Controls.Add(this.textBox10);
            this.groupBox2.Controls.Add(this.label5);
            this.groupBox2.Controls.Add(this.textBox7);
            this.groupBox2.Controls.Add(this.comboBox3);
            this.groupBox2.Controls.Add(this.textBox8);
            this.groupBox2.Controls.Add(this.label4);
            this.groupBox2.Controls.Add(this.textBox4);
            this.groupBox2.Controls.Add(this.comboBox2);
            this.groupBox2.Controls.Add(this.textBox6);
            this.groupBox2.Controls.Add(this.label3);
            this.groupBox2.Controls.Add(this.textBox2);
            this.groupBox2.Controls.Add(this.comboBox1);
            this.groupBox2.Controls.Add(this.textBox3);
            this.groupBox2.Controls.Add(this.label2);
            this.groupBox2.Controls.Add(this.textBox5);
            this.groupBox2.Controls.Add(this.label16);
            this.groupBox2.Controls.Add(this.comboBox_Interface_CMW500);
            this.groupBox2.Controls.Add(this.label7);
            this.groupBox2.Controls.Add(this.textBox1);
            this.groupBox2.Controls.Add(this.label1);
            this.groupBox2.Controls.Add(this.button4);
            this.groupBox2.Location = new System.Drawing.Point(327, 12);
            this.groupBox2.Name = "groupBox2";
            this.groupBox2.Size = new System.Drawing.Size(319, 215);
            this.groupBox2.TabIndex = 4;
            this.groupBox2.TabStop = false;
            this.groupBox2.Text = "GPIB";
            // 
            // button4
            // 
            this.button4.Location = new System.Drawing.Point(6, 26);
            this.button4.Name = "button4";
            this.button4.Size = new System.Drawing.Size(75, 20);
            this.button4.TabIndex = 1;
            this.button4.Text = "连接设备";
            this.button4.UseVisualStyleBackColor = true;
            // 
            // textBox1
            // 
            this.textBox1.Location = new System.Drawing.Point(203, 26);
            this.textBox1.Name = "textBox1";
            this.textBox1.Size = new System.Drawing.Size(40, 22);
            this.textBox1.TabIndex = 2;
            this.textBox1.Text = "1";
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(154, 31);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(43, 12);
            this.label1.TabIndex = 17;
            this.label1.Text = "34410C";
            // 
            // label16
            // 
            this.label16.AutoSize = true;
            this.label16.Location = new System.Drawing.Point(201, 11);
            this.label16.Name = "label16";
            this.label16.Size = new System.Drawing.Size(42, 12);
            this.label16.TabIndex = 22;
            this.label16.Text = "Address";
            // 
            // comboBox_Interface_CMW500
            // 
            this.comboBox_Interface_CMW500.FormattingEnabled = true;
            this.comboBox_Interface_CMW500.Items.AddRange(new object[] {
            "GPIB0",
            "GPIB1",
            "GPIB2",
            "GPIB3",
            "GPIB4",
            "GPIB5",
            "GPIB6",
            "GPIB7",
            "GPIB8",
            "GPIB9",
            "GPIB10",
            "GPIB11",
            "GPIB12",
            "GPIB13",
            "GPIB14",
            "GPIB15",
            "GPIB16",
            "GPIB17",
            "GPIB18",
            "GPIB19",
            "GPIB20"});
            this.comboBox_Interface_CMW500.Location = new System.Drawing.Point(87, 27);
            this.comboBox_Interface_CMW500.Name = "comboBox_Interface_CMW500";
            this.comboBox_Interface_CMW500.Size = new System.Drawing.Size(61, 20);
            this.comboBox_Interface_CMW500.TabIndex = 21;
            this.comboBox_Interface_CMW500.Text = "GPIB0";
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Location = new System.Drawing.Point(85, 12);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(55, 12);
            this.label7.TabIndex = 20;
            this.label7.Text = "   Interface";
            // 
            // textBox5
            // 
            this.textBox5.Location = new System.Drawing.Point(249, 26);
            this.textBox5.Name = "textBox5";
            this.textBox5.Size = new System.Drawing.Size(56, 22);
            this.textBox5.TabIndex = 23;
            this.textBox5.Text = "已连接";
            // 
            // textBox2
            // 
            this.textBox2.Location = new System.Drawing.Point(249, 52);
            this.textBox2.Name = "textBox2";
            this.textBox2.Size = new System.Drawing.Size(56, 22);
            this.textBox2.TabIndex = 27;
            this.textBox2.Text = "已连接";
            // 
            // comboBox1
            // 
            this.comboBox1.FormattingEnabled = true;
            this.comboBox1.Items.AddRange(new object[] {
            "GPIB0",
            "GPIB1",
            "GPIB2",
            "GPIB3",
            "GPIB4",
            "GPIB5",
            "GPIB6",
            "GPIB7",
            "GPIB8",
            "GPIB9",
            "GPIB10",
            "GPIB11",
            "GPIB12",
            "GPIB13",
            "GPIB14",
            "GPIB15",
            "GPIB16",
            "GPIB17",
            "GPIB18",
            "GPIB19",
            "GPIB20"});
            this.comboBox1.Location = new System.Drawing.Point(87, 53);
            this.comboBox1.Name = "comboBox1";
            this.comboBox1.Size = new System.Drawing.Size(61, 20);
            this.comboBox1.TabIndex = 26;
            this.comboBox1.Text = "GPIB0";
            // 
            // textBox3
            // 
            this.textBox3.Location = new System.Drawing.Point(203, 52);
            this.textBox3.Name = "textBox3";
            this.textBox3.Size = new System.Drawing.Size(40, 22);
            this.textBox3.TabIndex = 24;
            this.textBox3.Text = "1";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(154, 57);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(43, 12);
            this.label2.TabIndex = 25;
            this.label2.Text = "53230A";
            // 
            // textBox4
            // 
            this.textBox4.Location = new System.Drawing.Point(249, 78);
            this.textBox4.Name = "textBox4";
            this.textBox4.Size = new System.Drawing.Size(56, 22);
            this.textBox4.TabIndex = 31;
            this.textBox4.Text = "已连接";
            // 
            // comboBox2
            // 
            this.comboBox2.FormattingEnabled = true;
            this.comboBox2.Items.AddRange(new object[] {
            "GPIB0",
            "GPIB1",
            "GPIB2",
            "GPIB3",
            "GPIB4",
            "GPIB5",
            "GPIB6",
            "GPIB7",
            "GPIB8",
            "GPIB9",
            "GPIB10",
            "GPIB11",
            "GPIB12",
            "GPIB13",
            "GPIB14",
            "GPIB15",
            "GPIB16",
            "GPIB17",
            "GPIB18",
            "GPIB19",
            "GPIB20"});
            this.comboBox2.Location = new System.Drawing.Point(87, 79);
            this.comboBox2.Name = "comboBox2";
            this.comboBox2.Size = new System.Drawing.Size(61, 20);
            this.comboBox2.TabIndex = 30;
            this.comboBox2.Text = "GPIB0";
            // 
            // textBox6
            // 
            this.textBox6.Location = new System.Drawing.Point(203, 78);
            this.textBox6.Name = "textBox6";
            this.textBox6.Size = new System.Drawing.Size(40, 22);
            this.textBox6.TabIndex = 28;
            this.textBox6.Text = "1";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(154, 83);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(45, 12);
            this.label3.TabIndex = 29;
            this.label3.Text = "N1913A";
            // 
            // textBox7
            // 
            this.textBox7.Location = new System.Drawing.Point(249, 104);
            this.textBox7.Name = "textBox7";
            this.textBox7.Size = new System.Drawing.Size(56, 22);
            this.textBox7.TabIndex = 35;
            this.textBox7.Text = "已连接";
            // 
            // comboBox3
            // 
            this.comboBox3.FormattingEnabled = true;
            this.comboBox3.Items.AddRange(new object[] {
            "GPIB0",
            "GPIB1",
            "GPIB2",
            "GPIB3",
            "GPIB4",
            "GPIB5",
            "GPIB6",
            "GPIB7",
            "GPIB8",
            "GPIB9",
            "GPIB10",
            "GPIB11",
            "GPIB12",
            "GPIB13",
            "GPIB14",
            "GPIB15",
            "GPIB16",
            "GPIB17",
            "GPIB18",
            "GPIB19",
            "GPIB20"});
            this.comboBox3.Location = new System.Drawing.Point(87, 105);
            this.comboBox3.Name = "comboBox3";
            this.comboBox3.Size = new System.Drawing.Size(61, 20);
            this.comboBox3.TabIndex = 34;
            this.comboBox3.Text = "GPIB0";
            // 
            // textBox8
            // 
            this.textBox8.Location = new System.Drawing.Point(203, 104);
            this.textBox8.Name = "textBox8";
            this.textBox8.Size = new System.Drawing.Size(40, 22);
            this.textBox8.TabIndex = 32;
            this.textBox8.Text = "1";
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(154, 109);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(44, 12);
            this.label4.TabIndex = 33;
            this.label4.Text = "E4438C";
            // 
            // textBox9
            // 
            this.textBox9.Location = new System.Drawing.Point(249, 130);
            this.textBox9.Name = "textBox9";
            this.textBox9.Size = new System.Drawing.Size(56, 22);
            this.textBox9.TabIndex = 39;
            this.textBox9.Text = "已连接";
            // 
            // comboBox4
            // 
            this.comboBox4.FormattingEnabled = true;
            this.comboBox4.Items.AddRange(new object[] {
            "GPIB0",
            "GPIB1",
            "GPIB2",
            "GPIB3",
            "GPIB4",
            "GPIB5",
            "GPIB6",
            "GPIB7",
            "GPIB8",
            "GPIB9",
            "GPIB10",
            "GPIB11",
            "GPIB12",
            "GPIB13",
            "GPIB14",
            "GPIB15",
            "GPIB16",
            "GPIB17",
            "GPIB18",
            "GPIB19",
            "GPIB20"});
            this.comboBox4.Location = new System.Drawing.Point(87, 131);
            this.comboBox4.Name = "comboBox4";
            this.comboBox4.Size = new System.Drawing.Size(61, 20);
            this.comboBox4.TabIndex = 38;
            this.comboBox4.Text = "GPIB0";
            // 
            // textBox10
            // 
            this.textBox10.Location = new System.Drawing.Point(203, 130);
            this.textBox10.Name = "textBox10";
            this.textBox10.Size = new System.Drawing.Size(40, 22);
            this.textBox10.TabIndex = 36;
            this.textBox10.Text = "1";
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Location = new System.Drawing.Point(154, 135);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(45, 12);
            this.label5.TabIndex = 37;
            this.label5.Text = "N9030A";
            // 
            // textBox11
            // 
            this.textBox11.Location = new System.Drawing.Point(249, 156);
            this.textBox11.Name = "textBox11";
            this.textBox11.Size = new System.Drawing.Size(56, 22);
            this.textBox11.TabIndex = 43;
            this.textBox11.Text = "已连接";
            // 
            // comboBox5
            // 
            this.comboBox5.FormattingEnabled = true;
            this.comboBox5.Items.AddRange(new object[] {
            "GPIB0",
            "GPIB1",
            "GPIB2",
            "GPIB3",
            "GPIB4",
            "GPIB5",
            "GPIB6",
            "GPIB7",
            "GPIB8",
            "GPIB9",
            "GPIB10",
            "GPIB11",
            "GPIB12",
            "GPIB13",
            "GPIB14",
            "GPIB15",
            "GPIB16",
            "GPIB17",
            "GPIB18",
            "GPIB19",
            "GPIB20"});
            this.comboBox5.Location = new System.Drawing.Point(87, 157);
            this.comboBox5.Name = "comboBox5";
            this.comboBox5.Size = new System.Drawing.Size(61, 20);
            this.comboBox5.TabIndex = 42;
            this.comboBox5.Text = "GPIB0";
            // 
            // textBox12
            // 
            this.textBox12.Location = new System.Drawing.Point(203, 156);
            this.textBox12.Name = "textBox12";
            this.textBox12.Size = new System.Drawing.Size(40, 22);
            this.textBox12.TabIndex = 40;
            this.textBox12.Text = "1";
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Location = new System.Drawing.Point(154, 161);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(44, 12);
            this.label6.TabIndex = 41;
            this.label6.Text = "E5071C";
            // 
            // textBox13
            // 
            this.textBox13.Location = new System.Drawing.Point(249, 182);
            this.textBox13.Name = "textBox13";
            this.textBox13.Size = new System.Drawing.Size(56, 22);
            this.textBox13.TabIndex = 47;
            this.textBox13.Text = "已连接";
            // 
            // comboBox6
            // 
            this.comboBox6.FormattingEnabled = true;
            this.comboBox6.Items.AddRange(new object[] {
            "GPIB0",
            "GPIB1",
            "GPIB2",
            "GPIB3",
            "GPIB4",
            "GPIB5",
            "GPIB6",
            "GPIB7",
            "GPIB8",
            "GPIB9",
            "GPIB10",
            "GPIB11",
            "GPIB12",
            "GPIB13",
            "GPIB14",
            "GPIB15",
            "GPIB16",
            "GPIB17",
            "GPIB18",
            "GPIB19",
            "GPIB20"});
            this.comboBox6.Location = new System.Drawing.Point(87, 183);
            this.comboBox6.Name = "comboBox6";
            this.comboBox6.Size = new System.Drawing.Size(61, 20);
            this.comboBox6.TabIndex = 46;
            this.comboBox6.Text = "GPIB0";
            // 
            // textBox14
            // 
            this.textBox14.Location = new System.Drawing.Point(203, 182);
            this.textBox14.Name = "textBox14";
            this.textBox14.Size = new System.Drawing.Size(40, 22);
            this.textBox14.TabIndex = 44;
            this.textBox14.Text = "1";
            // 
            // label8
            // 
            this.label8.AutoSize = true;
            this.label8.Location = new System.Drawing.Point(154, 187);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(41, 12);
            this.label8.TabIndex = 45;
            this.label8.Text = "待测物";
            // 
            // main
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 12F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(659, 426);
            this.Controls.Add(this.groupBox2);
            this.Controls.Add(this.groupBox1);
            this.Name = "main";
            this.Text = "main";
            this.groupBox1.ResumeLayout(false);
            this.groupBox2.ResumeLayout(false);
            this.groupBox2.PerformLayout();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Button button1;
        private System.Windows.Forms.Button button2;
        private System.Windows.Forms.Button button3;
        private System.Windows.Forms.GroupBox groupBox1;
        private System.Windows.Forms.GroupBox groupBox2;
        private System.Windows.Forms.TextBox textBox1;
        private System.Windows.Forms.Button button4;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label label16;
        private System.Windows.Forms.ComboBox comboBox_Interface_CMW500;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.TextBox textBox5;
        private System.Windows.Forms.TextBox textBox13;
        private System.Windows.Forms.ComboBox comboBox6;
        private System.Windows.Forms.TextBox textBox14;
        private System.Windows.Forms.Label label8;
        private System.Windows.Forms.TextBox textBox11;
        private System.Windows.Forms.ComboBox comboBox5;
        private System.Windows.Forms.TextBox textBox12;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.TextBox textBox9;
        private System.Windows.Forms.ComboBox comboBox4;
        private System.Windows.Forms.TextBox textBox10;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.TextBox textBox7;
        private System.Windows.Forms.ComboBox comboBox3;
        private System.Windows.Forms.TextBox textBox8;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.TextBox textBox4;
        private System.Windows.Forms.ComboBox comboBox2;
        private System.Windows.Forms.TextBox textBox6;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.TextBox textBox2;
        private System.Windows.Forms.ComboBox comboBox1;
        private System.Windows.Forms.TextBox textBox3;
        private System.Windows.Forms.Label label2;
    }
}