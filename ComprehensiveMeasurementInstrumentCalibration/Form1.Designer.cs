﻿namespace ComprehensiveMeasurementInstrumentCalibration
{
    partial class Form1
    {
        /// <summary>
        /// 必需的设计器变量。
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// 清理所有正在使用的资源。
        /// </summary>
        /// <param name="disposing">如果应释放托管资源，为 true；否则为 false。</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows 窗体设计器生成的代码

        /// <summary>
        /// 设计器支持所需的方法 - 不要
        /// 使用代码编辑器修改此方法的内容。
        /// </summary>
        private void InitializeComponent()
        {
            this.button_InstrumentIDN_CMW500 = new System.Windows.Forms.Button();
            this.comboBox_Address_CMW500 = new System.Windows.Forms.ComboBox();
            this.label16 = new System.Windows.Forms.Label();
            this.comboBox_Interface_CMW500 = new System.Windows.Forms.ComboBox();
            this.label7 = new System.Windows.Forms.Label();
            this.button_InstrumentIDN_N9030A = new System.Windows.Forms.Button();
            this.comboBox_Address_N9030A = new System.Windows.Forms.ComboBox();
            this.label1 = new System.Windows.Forms.Label();
            this.comboBox_Interface_N9030A = new System.Windows.Forms.ComboBox();
            this.label2 = new System.Windows.Forms.Label();
            this.button_TXfrequencyAccuracy = new System.Windows.Forms.Button();
            this.textBox_CMW500_Massage = new System.Windows.Forms.TextBox();
            this.textBox_N9030A_Massage = new System.Windows.Forms.TextBox();
            this.textBox_53230A_Massage = new System.Windows.Forms.TextBox();
            this.button_InstrumentIDN_53230A = new System.Windows.Forms.Button();
            this.comboBox_Address_53230A = new System.Windows.Forms.ComboBox();
            this.label3 = new System.Windows.Forms.Label();
            this.comboBox_Interface_53230A = new System.Windows.Forms.ComboBox();
            this.label4 = new System.Windows.Forms.Label();
            this.groupBox_frequency = new System.Windows.Forms.GroupBox();
            this.textBox_frequencySectionInputStep = new System.Windows.Forms.TextBox();
            this.textBox_frequencySectionInputEnd = new System.Windows.Forms.TextBox();
            this.textBox_frequencySectionInputStart = new System.Windows.Forms.TextBox();
            this.label14 = new System.Windows.Forms.Label();
            this.label13 = new System.Windows.Forms.Label();
            this.label11 = new System.Windows.Forms.Label();
            this.radioButton_frequencySectionInput = new System.Windows.Forms.RadioButton();
            this.radioButton_frequencySingleInput = new System.Windows.Forms.RadioButton();
            this.textBox_frequencySingleInput = new System.Windows.Forms.TextBox();
            this.groupBox_level = new System.Windows.Forms.GroupBox();
            this.textBox_levelSectionInputStep = new System.Windows.Forms.TextBox();
            this.textBox_levelSectionInputEnd = new System.Windows.Forms.TextBox();
            this.textBox_levelSectionInputStart = new System.Windows.Forms.TextBox();
            this.label8 = new System.Windows.Forms.Label();
            this.label9 = new System.Windows.Forms.Label();
            this.label10 = new System.Windows.Forms.Label();
            this.radioButton_levelSectionInput = new System.Windows.Forms.RadioButton();
            this.radioButton_levelSingleInput = new System.Windows.Forms.RadioButton();
            this.textBox6 = new System.Windows.Forms.TextBox();
            this.groupBox_frequency.SuspendLayout();
            this.groupBox_level.SuspendLayout();
            this.SuspendLayout();
            // 
            // button_InstrumentIDN_CMW500
            // 
            this.button_InstrumentIDN_CMW500.Location = new System.Drawing.Point(231, 13);
            this.button_InstrumentIDN_CMW500.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.button_InstrumentIDN_CMW500.Name = "button_InstrumentIDN_CMW500";
            this.button_InstrumentIDN_CMW500.Size = new System.Drawing.Size(152, 26);
            this.button_InstrumentIDN_CMW500.TabIndex = 10;
            this.button_InstrumentIDN_CMW500.Text = "点此处连接CMW500";
            this.button_InstrumentIDN_CMW500.UseVisualStyleBackColor = true;
            this.button_InstrumentIDN_CMW500.Click += new System.EventHandler(this.button_InstrumentIDN_CMW500_Click);
            // 
            // comboBox_Address_CMW500
            // 
            this.comboBox_Address_CMW500.FormattingEnabled = true;
            this.comboBox_Address_CMW500.Items.AddRange(new object[] {
            "0",
            "1",
            "2",
            "3",
            "4",
            "5",
            "6",
            "7",
            "8",
            "9",
            "10",
            "11",
            "12",
            "13",
            "14",
            "15",
            "16",
            "17",
            "18",
            "19",
            "20",
            "21",
            "22",
            "23",
            "24",
            "25",
            "26",
            "27",
            "28",
            "29",
            "30",
            "31"});
            this.comboBox_Address_CMW500.Location = new System.Drawing.Point(162, 17);
            this.comboBox_Address_CMW500.Name = "comboBox_Address_CMW500";
            this.comboBox_Address_CMW500.Size = new System.Drawing.Size(61, 20);
            this.comboBox_Address_CMW500.TabIndex = 12;
            this.comboBox_Address_CMW500.Text = "20";
            // 
            // label16
            // 
            this.label16.AutoSize = true;
            this.label16.Location = new System.Drawing.Point(119, 20);
            this.label16.Name = "label16";
            this.label16.Size = new System.Drawing.Size(42, 12);
            this.label16.TabIndex = 11;
            this.label16.Text = "Address";
            // 
            // comboBox_Interface_CMW500
            // 
            this.comboBox_Interface_CMW500.FormattingEnabled = true;
            this.comboBox_Interface_CMW500.Items.AddRange(new object[] {
            "GPIB0",
            "GPIB1",
            "GPIB2",
            "GPIB3",
            "GPIB4",
            "GPIB5",
            "GPIB6",
            "GPIB7",
            "GPIB8",
            "GPIB9",
            "GPIB10",
            "GPIB11",
            "GPIB12",
            "GPIB13",
            "GPIB14",
            "GPIB15",
            "GPIB16",
            "GPIB17",
            "GPIB18",
            "GPIB19",
            "GPIB20"});
            this.comboBox_Interface_CMW500.Location = new System.Drawing.Point(52, 17);
            this.comboBox_Interface_CMW500.Name = "comboBox_Interface_CMW500";
            this.comboBox_Interface_CMW500.Size = new System.Drawing.Size(61, 20);
            this.comboBox_Interface_CMW500.TabIndex = 9;
            this.comboBox_Interface_CMW500.Text = "GPIB0";
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Location = new System.Drawing.Point(8, 20);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(46, 12);
            this.label7.TabIndex = 8;
            this.label7.Text = "Interface";
            // 
            // button_InstrumentIDN_N9030A
            // 
            this.button_InstrumentIDN_N9030A.Location = new System.Drawing.Point(231, 75);
            this.button_InstrumentIDN_N9030A.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.button_InstrumentIDN_N9030A.Name = "button_InstrumentIDN_N9030A";
            this.button_InstrumentIDN_N9030A.Size = new System.Drawing.Size(152, 26);
            this.button_InstrumentIDN_N9030A.TabIndex = 15;
            this.button_InstrumentIDN_N9030A.Text = "点此处连接N9030A";
            this.button_InstrumentIDN_N9030A.UseVisualStyleBackColor = true;
            this.button_InstrumentIDN_N9030A.Click += new System.EventHandler(this.button_InstrumentIDN_N9030A_Click);
            // 
            // comboBox_Address_N9030A
            // 
            this.comboBox_Address_N9030A.FormattingEnabled = true;
            this.comboBox_Address_N9030A.Items.AddRange(new object[] {
            "0",
            "1",
            "2",
            "3",
            "4",
            "5",
            "6",
            "7",
            "8",
            "9",
            "10",
            "11",
            "12",
            "13",
            "14",
            "15",
            "16",
            "17",
            "18",
            "19",
            "20",
            "21",
            "22",
            "23",
            "24",
            "25",
            "26",
            "27",
            "28",
            "29",
            "30",
            "31"});
            this.comboBox_Address_N9030A.Location = new System.Drawing.Point(163, 79);
            this.comboBox_Address_N9030A.Name = "comboBox_Address_N9030A";
            this.comboBox_Address_N9030A.Size = new System.Drawing.Size(61, 20);
            this.comboBox_Address_N9030A.TabIndex = 17;
            this.comboBox_Address_N9030A.Text = "18";
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(120, 82);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(42, 12);
            this.label1.TabIndex = 16;
            this.label1.Text = "Address";
            // 
            // comboBox_Interface_N9030A
            // 
            this.comboBox_Interface_N9030A.FormattingEnabled = true;
            this.comboBox_Interface_N9030A.Items.AddRange(new object[] {
            "GPIB0",
            "GPIB1",
            "GPIB2",
            "GPIB3",
            "GPIB4",
            "GPIB5",
            "GPIB6",
            "GPIB7",
            "GPIB8",
            "GPIB9",
            "GPIB10",
            "GPIB11",
            "GPIB12",
            "GPIB13",
            "GPIB14",
            "GPIB15",
            "GPIB16",
            "GPIB17",
            "GPIB18",
            "GPIB19",
            "GPIB20"});
            this.comboBox_Interface_N9030A.Location = new System.Drawing.Point(53, 79);
            this.comboBox_Interface_N9030A.Name = "comboBox_Interface_N9030A";
            this.comboBox_Interface_N9030A.Size = new System.Drawing.Size(61, 20);
            this.comboBox_Interface_N9030A.TabIndex = 14;
            this.comboBox_Interface_N9030A.Text = "GPIB0";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(9, 82);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(46, 12);
            this.label2.TabIndex = 13;
            this.label2.Text = "Interface";
            // 
            // button_TXfrequencyAccuracy
            // 
            this.button_TXfrequencyAccuracy.Location = new System.Drawing.Point(13, 198);
            this.button_TXfrequencyAccuracy.Name = "button_TXfrequencyAccuracy";
            this.button_TXfrequencyAccuracy.Size = new System.Drawing.Size(149, 23);
            this.button_TXfrequencyAccuracy.TabIndex = 19;
            this.button_TXfrequencyAccuracy.Text = "CMW500频率精准度校准";
            this.button_TXfrequencyAccuracy.UseVisualStyleBackColor = true;
            this.button_TXfrequencyAccuracy.Click += new System.EventHandler(this.button_TXfrequencyAccuracy_Click);
            // 
            // textBox_CMW500_Massage
            // 
            this.textBox_CMW500_Massage.Location = new System.Drawing.Point(10, 46);
            this.textBox_CMW500_Massage.Name = "textBox_CMW500_Massage";
            this.textBox_CMW500_Massage.Size = new System.Drawing.Size(373, 22);
            this.textBox_CMW500_Massage.TabIndex = 20;
            // 
            // textBox_N9030A_Massage
            // 
            this.textBox_N9030A_Massage.Location = new System.Drawing.Point(11, 108);
            this.textBox_N9030A_Massage.Name = "textBox_N9030A_Massage";
            this.textBox_N9030A_Massage.Size = new System.Drawing.Size(373, 22);
            this.textBox_N9030A_Massage.TabIndex = 21;
            // 
            // textBox_53230A_Massage
            // 
            this.textBox_53230A_Massage.Location = new System.Drawing.Point(12, 170);
            this.textBox_53230A_Massage.Name = "textBox_53230A_Massage";
            this.textBox_53230A_Massage.Size = new System.Drawing.Size(373, 22);
            this.textBox_53230A_Massage.TabIndex = 27;
            // 
            // button_InstrumentIDN_53230A
            // 
            this.button_InstrumentIDN_53230A.Location = new System.Drawing.Point(232, 137);
            this.button_InstrumentIDN_53230A.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.button_InstrumentIDN_53230A.Name = "button_InstrumentIDN_53230A";
            this.button_InstrumentIDN_53230A.Size = new System.Drawing.Size(152, 26);
            this.button_InstrumentIDN_53230A.TabIndex = 24;
            this.button_InstrumentIDN_53230A.Text = "点此处连接53230A";
            this.button_InstrumentIDN_53230A.UseVisualStyleBackColor = true;
            this.button_InstrumentIDN_53230A.Click += new System.EventHandler(this.button_InstrumentIDN_53230A_Click);
            // 
            // comboBox_Address_53230A
            // 
            this.comboBox_Address_53230A.FormattingEnabled = true;
            this.comboBox_Address_53230A.Items.AddRange(new object[] {
            "0",
            "1",
            "2",
            "3",
            "4",
            "5",
            "6",
            "7",
            "8",
            "9",
            "10",
            "11",
            "12",
            "13",
            "14",
            "15",
            "16",
            "17",
            "18",
            "19",
            "20",
            "21",
            "22",
            "23",
            "24",
            "25",
            "26",
            "27",
            "28",
            "29",
            "30",
            "31"});
            this.comboBox_Address_53230A.Location = new System.Drawing.Point(164, 141);
            this.comboBox_Address_53230A.Name = "comboBox_Address_53230A";
            this.comboBox_Address_53230A.Size = new System.Drawing.Size(61, 20);
            this.comboBox_Address_53230A.TabIndex = 26;
            this.comboBox_Address_53230A.Text = "4";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(120, 144);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(42, 12);
            this.label3.TabIndex = 25;
            this.label3.Text = "Address";
            // 
            // comboBox_Interface_53230A
            // 
            this.comboBox_Interface_53230A.FormattingEnabled = true;
            this.comboBox_Interface_53230A.Items.AddRange(new object[] {
            "GPIB0",
            "GPIB1",
            "GPIB2",
            "GPIB3",
            "GPIB4",
            "GPIB5",
            "GPIB6",
            "GPIB7",
            "GPIB8",
            "GPIB9",
            "GPIB10",
            "GPIB11",
            "GPIB12",
            "GPIB13",
            "GPIB14",
            "GPIB15",
            "GPIB16",
            "GPIB17",
            "GPIB18",
            "GPIB19",
            "GPIB20"});
            this.comboBox_Interface_53230A.Location = new System.Drawing.Point(54, 141);
            this.comboBox_Interface_53230A.Name = "comboBox_Interface_53230A";
            this.comboBox_Interface_53230A.Size = new System.Drawing.Size(61, 20);
            this.comboBox_Interface_53230A.TabIndex = 23;
            this.comboBox_Interface_53230A.Text = "GPIB0";
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(10, 144);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(46, 12);
            this.label4.TabIndex = 22;
            this.label4.Text = "Interface";
            // 
            // groupBox_frequency
            // 
            this.groupBox_frequency.Controls.Add(this.textBox_frequencySectionInputStep);
            this.groupBox_frequency.Controls.Add(this.textBox_frequencySectionInputEnd);
            this.groupBox_frequency.Controls.Add(this.textBox_frequencySectionInputStart);
            this.groupBox_frequency.Controls.Add(this.label14);
            this.groupBox_frequency.Controls.Add(this.label13);
            this.groupBox_frequency.Controls.Add(this.label11);
            this.groupBox_frequency.Controls.Add(this.radioButton_frequencySectionInput);
            this.groupBox_frequency.Controls.Add(this.radioButton_frequencySingleInput);
            this.groupBox_frequency.Controls.Add(this.textBox_frequencySingleInput);
            this.groupBox_frequency.Location = new System.Drawing.Point(13, 227);
            this.groupBox_frequency.Name = "groupBox_frequency";
            this.groupBox_frequency.Size = new System.Drawing.Size(365, 77);
            this.groupBox_frequency.TabIndex = 32;
            this.groupBox_frequency.TabStop = false;
            this.groupBox_frequency.Text = "频率(MHz)输入";
            // 
            // textBox_frequencySectionInputStep
            // 
            this.textBox_frequencySectionInputStep.Location = new System.Drawing.Point(300, 49);
            this.textBox_frequencySectionInputStep.Name = "textBox_frequencySectionInputStep";
            this.textBox_frequencySectionInputStep.Size = new System.Drawing.Size(51, 22);
            this.textBox_frequencySectionInputStep.TabIndex = 20;
            // 
            // textBox_frequencySectionInputEnd
            // 
            this.textBox_frequencySectionInputEnd.Location = new System.Drawing.Point(210, 49);
            this.textBox_frequencySectionInputEnd.Name = "textBox_frequencySectionInputEnd";
            this.textBox_frequencySectionInputEnd.Size = new System.Drawing.Size(57, 22);
            this.textBox_frequencySectionInputEnd.TabIndex = 19;
            // 
            // textBox_frequencySectionInputStart
            // 
            this.textBox_frequencySectionInputStart.Location = new System.Drawing.Point(116, 49);
            this.textBox_frequencySectionInputStart.Name = "textBox_frequencySectionInputStart";
            this.textBox_frequencySectionInputStart.Size = new System.Drawing.Size(58, 22);
            this.textBox_frequencySectionInputStart.TabIndex = 16;
            // 
            // label14
            // 
            this.label14.AutoSize = true;
            this.label14.Location = new System.Drawing.Point(273, 52);
            this.label14.Name = "label14";
            this.label14.Size = new System.Drawing.Size(25, 12);
            this.label14.TabIndex = 18;
            this.label14.Text = "Step";
            // 
            // label13
            // 
            this.label13.AutoSize = true;
            this.label13.Location = new System.Drawing.Point(180, 52);
            this.label13.Name = "label13";
            this.label13.Size = new System.Drawing.Size(24, 12);
            this.label13.TabIndex = 17;
            this.label13.Text = "End";
            // 
            // label11
            // 
            this.label11.AutoSize = true;
            this.label11.Location = new System.Drawing.Point(85, 52);
            this.label11.Name = "label11";
            this.label11.Size = new System.Drawing.Size(26, 12);
            this.label11.TabIndex = 16;
            this.label11.Text = "Start";
            // 
            // radioButton_frequencySectionInput
            // 
            this.radioButton_frequencySectionInput.AutoSize = true;
            this.radioButton_frequencySectionInput.Location = new System.Drawing.Point(8, 50);
            this.radioButton_frequencySectionInput.Name = "radioButton_frequencySectionInput";
            this.radioButton_frequencySectionInput.Size = new System.Drawing.Size(71, 16);
            this.radioButton_frequencySectionInput.TabIndex = 15;
            this.radioButton_frequencySectionInput.Text = "区间输入";
            this.radioButton_frequencySectionInput.UseVisualStyleBackColor = true;
            // 
            // radioButton_frequencySingleInput
            // 
            this.radioButton_frequencySingleInput.AutoSize = true;
            this.radioButton_frequencySingleInput.Checked = true;
            this.radioButton_frequencySingleInput.Location = new System.Drawing.Point(8, 23);
            this.radioButton_frequencySingleInput.Name = "radioButton_frequencySingleInput";
            this.radioButton_frequencySingleInput.Size = new System.Drawing.Size(94, 16);
            this.radioButton_frequencySingleInput.TabIndex = 14;
            this.radioButton_frequencySingleInput.TabStop = true;
            this.radioButton_frequencySingleInput.Text = "分割符(,)输入";
            this.radioButton_frequencySingleInput.UseVisualStyleBackColor = true;
            // 
            // textBox_frequencySingleInput
            // 
            this.textBox_frequencySingleInput.Location = new System.Drawing.Point(108, 21);
            this.textBox_frequencySingleInput.Name = "textBox_frequencySingleInput";
            this.textBox_frequencySingleInput.Size = new System.Drawing.Size(245, 22);
            this.textBox_frequencySingleInput.TabIndex = 13;
            this.textBox_frequencySingleInput.Text = "200";
            // 
            // groupBox_level
            // 
            this.groupBox_level.Controls.Add(this.textBox_levelSectionInputStep);
            this.groupBox_level.Controls.Add(this.textBox_levelSectionInputEnd);
            this.groupBox_level.Controls.Add(this.textBox_levelSectionInputStart);
            this.groupBox_level.Controls.Add(this.label8);
            this.groupBox_level.Controls.Add(this.label9);
            this.groupBox_level.Controls.Add(this.label10);
            this.groupBox_level.Controls.Add(this.radioButton_levelSectionInput);
            this.groupBox_level.Controls.Add(this.radioButton_levelSingleInput);
            this.groupBox_level.Controls.Add(this.textBox6);
            this.groupBox_level.Location = new System.Drawing.Point(13, 310);
            this.groupBox_level.Name = "groupBox_level";
            this.groupBox_level.Size = new System.Drawing.Size(365, 77);
            this.groupBox_level.TabIndex = 33;
            this.groupBox_level.TabStop = false;
            this.groupBox_level.Text = "电平(dBm)输入";
            // 
            // textBox_levelSectionInputStep
            // 
            this.textBox_levelSectionInputStep.Location = new System.Drawing.Point(300, 49);
            this.textBox_levelSectionInputStep.Name = "textBox_levelSectionInputStep";
            this.textBox_levelSectionInputStep.Size = new System.Drawing.Size(51, 22);
            this.textBox_levelSectionInputStep.TabIndex = 20;
            // 
            // textBox_levelSectionInputEnd
            // 
            this.textBox_levelSectionInputEnd.Location = new System.Drawing.Point(210, 49);
            this.textBox_levelSectionInputEnd.Name = "textBox_levelSectionInputEnd";
            this.textBox_levelSectionInputEnd.Size = new System.Drawing.Size(57, 22);
            this.textBox_levelSectionInputEnd.TabIndex = 19;
            // 
            // textBox_levelSectionInputStart
            // 
            this.textBox_levelSectionInputStart.Location = new System.Drawing.Point(116, 49);
            this.textBox_levelSectionInputStart.Name = "textBox_levelSectionInputStart";
            this.textBox_levelSectionInputStart.Size = new System.Drawing.Size(58, 22);
            this.textBox_levelSectionInputStart.TabIndex = 16;
            // 
            // label8
            // 
            this.label8.AutoSize = true;
            this.label8.Location = new System.Drawing.Point(273, 52);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(25, 12);
            this.label8.TabIndex = 18;
            this.label8.Text = "Step";
            // 
            // label9
            // 
            this.label9.AutoSize = true;
            this.label9.Location = new System.Drawing.Point(180, 52);
            this.label9.Name = "label9";
            this.label9.Size = new System.Drawing.Size(24, 12);
            this.label9.TabIndex = 17;
            this.label9.Text = "End";
            // 
            // label10
            // 
            this.label10.AutoSize = true;
            this.label10.Location = new System.Drawing.Point(85, 52);
            this.label10.Name = "label10";
            this.label10.Size = new System.Drawing.Size(26, 12);
            this.label10.TabIndex = 16;
            this.label10.Text = "Start";
            // 
            // radioButton_levelSectionInput
            // 
            this.radioButton_levelSectionInput.AutoSize = true;
            this.radioButton_levelSectionInput.Location = new System.Drawing.Point(8, 50);
            this.radioButton_levelSectionInput.Name = "radioButton_levelSectionInput";
            this.radioButton_levelSectionInput.Size = new System.Drawing.Size(71, 16);
            this.radioButton_levelSectionInput.TabIndex = 15;
            this.radioButton_levelSectionInput.Text = "区间输入";
            this.radioButton_levelSectionInput.UseVisualStyleBackColor = true;
            // 
            // radioButton_levelSingleInput
            // 
            this.radioButton_levelSingleInput.AutoSize = true;
            this.radioButton_levelSingleInput.Checked = true;
            this.radioButton_levelSingleInput.Location = new System.Drawing.Point(8, 23);
            this.radioButton_levelSingleInput.Name = "radioButton_levelSingleInput";
            this.radioButton_levelSingleInput.Size = new System.Drawing.Size(94, 16);
            this.radioButton_levelSingleInput.TabIndex = 14;
            this.radioButton_levelSingleInput.TabStop = true;
            this.radioButton_levelSingleInput.Text = "分割符(,)输入";
            this.radioButton_levelSingleInput.UseVisualStyleBackColor = true;
            // 
            // textBox6
            // 
            this.textBox6.Location = new System.Drawing.Point(108, 21);
            this.textBox6.Name = "textBox6";
            this.textBox6.Size = new System.Drawing.Size(245, 22);
            this.textBox6.TabIndex = 13;
            this.textBox6.Text = "-5";
            // 
            // Form1
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 12F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(413, 409);
            this.Controls.Add(this.groupBox_level);
            this.Controls.Add(this.groupBox_frequency);
            this.Controls.Add(this.textBox_53230A_Massage);
            this.Controls.Add(this.button_InstrumentIDN_53230A);
            this.Controls.Add(this.comboBox_Address_53230A);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.comboBox_Interface_53230A);
            this.Controls.Add(this.label4);
            this.Controls.Add(this.textBox_N9030A_Massage);
            this.Controls.Add(this.textBox_CMW500_Massage);
            this.Controls.Add(this.button_TXfrequencyAccuracy);
            this.Controls.Add(this.button_InstrumentIDN_N9030A);
            this.Controls.Add(this.comboBox_Address_N9030A);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.comboBox_Interface_N9030A);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.button_InstrumentIDN_CMW500);
            this.Controls.Add(this.comboBox_Address_CMW500);
            this.Controls.Add(this.label16);
            this.Controls.Add(this.comboBox_Interface_CMW500);
            this.Controls.Add(this.label7);
            this.Name = "Form1";
            this.Text = " ";
            this.groupBox_frequency.ResumeLayout(false);
            this.groupBox_frequency.PerformLayout();
            this.groupBox_level.ResumeLayout(false);
            this.groupBox_level.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Button button_InstrumentIDN_CMW500;
        private System.Windows.Forms.ComboBox comboBox_Address_CMW500;
        private System.Windows.Forms.Label label16;
        private System.Windows.Forms.ComboBox comboBox_Interface_CMW500;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.Button button_InstrumentIDN_N9030A;
        private System.Windows.Forms.ComboBox comboBox_Address_N9030A;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.ComboBox comboBox_Interface_N9030A;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Button button_TXfrequencyAccuracy;
        private System.Windows.Forms.TextBox textBox_CMW500_Massage;
        private System.Windows.Forms.TextBox textBox_N9030A_Massage;
        private System.Windows.Forms.TextBox textBox_53230A_Massage;
        private System.Windows.Forms.Button button_InstrumentIDN_53230A;
        private System.Windows.Forms.ComboBox comboBox_Address_53230A;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.ComboBox comboBox_Interface_53230A;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.GroupBox groupBox_frequency;
        private System.Windows.Forms.TextBox textBox_frequencySectionInputStep;
        private System.Windows.Forms.TextBox textBox_frequencySectionInputEnd;
        private System.Windows.Forms.TextBox textBox_frequencySectionInputStart;
        private System.Windows.Forms.Label label14;
        private System.Windows.Forms.Label label13;
        private System.Windows.Forms.Label label11;
        private System.Windows.Forms.RadioButton radioButton_frequencySectionInput;
        private System.Windows.Forms.RadioButton radioButton_frequencySingleInput;
        private System.Windows.Forms.TextBox textBox_frequencySingleInput;
        private System.Windows.Forms.GroupBox groupBox_level;
        private System.Windows.Forms.TextBox textBox_levelSectionInputStep;
        private System.Windows.Forms.TextBox textBox_levelSectionInputEnd;
        private System.Windows.Forms.TextBox textBox_levelSectionInputStart;
        private System.Windows.Forms.Label label8;
        private System.Windows.Forms.Label label9;
        private System.Windows.Forms.Label label10;
        private System.Windows.Forms.RadioButton radioButton_levelSectionInput;
        private System.Windows.Forms.RadioButton radioButton_levelSingleInput;
        private System.Windows.Forms.TextBox textBox6;
    }
}

