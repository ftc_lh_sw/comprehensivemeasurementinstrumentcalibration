﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ComprehensiveMeasurementInstrumentCalibration
{
    class SCPI_Commands
    {
        //===== General Purpose RF Generator =====
        public const string CMW500_GPRF_GEN1_STATE = "SOURce:GPRF:GEN1:STATe";      




        // =================================//
        public const string RESET = "*RST";

        public const string CMW500_LTE_SIGNALING_CELL_STATE = "SOURce:LTE:SIGN:CELL:STATe";

        public const string CMW270_WLAN_SIGNALING_STATE = "SOURce:WLAN:SIGN:STATe";

        public const string CMW270_WLAN_SIGN_RFS_FREQ = "CONF:WLAN:SIGN:RFS:FREQ";

        public const string CMW270_WLAN_SIGN_RFS_TXBurstPower = "CONF:WLAN:SIGN:RFS:BOP";

        public const string CMW270_WLAN_SIGN_RFS_RXExpectedPEP = "CONF:WLAN:SIGN:RFS:EPEP";

        public const string CMW270_WLAN_SIGN_Connenction_Status = "FETCh:WLAN:SIGNaling:PSWitched:STATe";

        public const string CMW270_WLAN_SIGN_PER_STAT = "FETC:WLAN:SIGN:PER:STAT";

        public const string CMW270_WLAN_SIGN_PER_INIT = "INIT:WLAN:SIGN:PER";

        public const string CMW270_WLAN_SIGN_PER_ABORT = "ABORT:WLAN:SIGN:PER";


        public const string CMW270_WLAN_SIGN_PER_VALUE = "FETC:WLAN:SIGN:PER";

        public const string CMW270_WLAN_SIGN_PER_PACK = "CONF:WLAN:SIGN:PER:PACK";

        public const string CMW270_WLAN_SIGNALING_SCENARIO = "ROUTe:WLAN:SIGN:SCENario:SCELl";


        //===== LTE Signaling Configuration: RF Settings =====  
        public const string CMW500_LTE_SIGNALING_DMODE = "CONFigure:LTE:SIGN:DMODe";     //FDDorTDD

        public const string CMW500_LTE_SIGNALING_SCENARIO = "ROUTe:LTE:SIGN:SCENario:SCELl";

        public const string CMW500_LTE_SIGNALING_RFSETTINGS_EATTENUATION_OUTPUT = "CONFigure:LTE:SIGN:RFSettings:EATTenuation:OUTPut";

        public const string CMW270_WLAN_SIGNALING_RFSETTINGS_EATTENUATION_OUTPUT = "CONFigure:WLAN:SIGN:RFSettings:EATTenuation:OUTPut";

        public const string CMW500_LTE_SIGNALING_RFSETTINGS_EATTENUATION_INPUT = "CONFigure:LTE:SIGN:RFSettings:EATTenuation:INPut";

        public const string CMW270_WLAN_SIGNALING_RFSETTINGS_EATTENUATION_INPUT = "CONFigure:WLAN:SIGN:RFSettings:EATTenuation:INPut";

        public const string CMW500_LTE_SIGNALING_OPERATING_BAND = "CONFigure:LTE:SIGN:BAND";

        public const string CMW500_LTE_SIGNALING_CHANNEL_DL = "CONFigure:LTE:SIGN:RFSettings:CHANnel:DL";

        public const string CMW500_LTE_SIGNALING_CHANNEL_UL = "CONFigure:LTE:SIGN:RFSettings:CHANnel:UL";

        public const string CMW500_LTE_SIGNALING_FREQUENCY_DL = "CONFigure:LTE:SIGN:RFSettings:FREQuency:DL";

        public const string CMW500_LTE_SIGNALING_FREQUENCY_UL = "CONFigure:LTE:SIGN:RFSettings:FREQuency:UL";

        public const string CMW500_LTE_SIGNALING_EXP_NOMINAL_POWER_MODE = "CONFigure:LTE:SIGN:RFSettings:ENPMode";

        public const string CMW500_LTE_SIGNALING_EXP_NOMINAL_POWER = "CONFigure:LTE:SIGN:RFSettings:ENPower";

        public const string CMW500_LTE_SIGNALING_MARGIN = "CONFigure:LTE:SIGN:RFSettings:UMARgin";


        //===== LTE Signaling Configuration: Network : Security Settings =====
        public const string CMW500_LTE_SIGNALING_SECURITY_AUTHENTICATION = "CONFigure:LTE:SIGN:CELL:SECurity:AUTHenticat";

        public const string CMW500_LTE_SIGNALING_SECURITY_NAS = "CONFigure:LTE:SIGN:CELL:SECurity:NAS";

        public const string CMW500_LTE_SIGNALING_SECURITY_AS = "CONFigure:LTE:SIGN:CELL:SECurity:AS";

        public const string CMW500_LTE_SIGNALING_SECURITY_MILENAGE = "CONFigure:LTE:SIGN:CELL:SECurity:MILenage";

        public const string CMW500_LTE_SIGNALING_SECURITY_OPC = "CONFigure:LTE:SIGN:CELL:SECurity:OPC";

        public const string CMW500_LTE_SIGNALING_SECURITY_SKEY = "CONFigure:LTE:SIGN:CELL:SECurity:SKEY";


        public const string CMW500_LTE_SIGNALING_SECURITY_IALGORITHM = "CONFigure:LTE:SIGN:CELL:SECurity:IALGorithm";


        // ===== LTE Signaling Configuration: Uplink Power Control =====
        public const string CMW500_LTE_SIGNALING_PUSCH_LEVEL = "CONFigure:LTE:SIGN:UL:PUSCh:LEVel";

        public const string CMW500_LTE_SIGNALING_UL_TPC_CLTPOWER = "CONFigure:LTE:Sign:UPLink:TPC:CLTPower";

        public const string CMW500_LTE_SIGNALING_UL_TPC = "CONFigure:LTE:SIGN:UL:TPC:SET";

        public const string CMW500_LTE_SIGNALING_UL_TPC_SINGLE = "CONFigure:LTE:SIGN:UL:TPC:SINGle";

        public const string CMW500_LTE_SIGNALING_UL_PUSCH_TPC_USER_DEFINED_PATTERN = "CONFigure:LTE:SIGN:UL:PUSCh:TPC:UDPattern";

        public const string CMW500_LTE_SIGNALING_UL_PUCCH_CLTPOWER = "CONFigure:LTE:Sign:UL:PUCCh:CLTPower";

        public const string CMW500_LTE_SIGNALING_UL_PMAX = "CONFigure:LTE:SIGN:UL:PMAX";

          
        // ===== Cell Setup =====      單元設置
        public const string CMW500_LTE_SIGNALING_DL_RS_EPRE = "CONFigure:LTE:SIGN:DL:RSEPre:LEVel";

        public const string CMW500_LTE_SIGNALING_CELL_BANDWIDTH_DL = "CONFigure:LTE:SIGN:CELL:BANDwidth:DL";

        public const string CMW500_LTE_SIGNALING_CELL_BANDWIDTH_UL = "CONFigure:LTE:SIGN:CELL:BANDwidth:UL";


        // ===== Connection Status =====  連接狀態
        public const string CONNECTION_STATUS_CONNECTION_ON = "ON";

        public const string CONNECTION_STATUS_ATTACHED = "Attached";
        public const string CONNECTION_STATUS_CONNECTION_ESTABLISHED = "Connection Established";
        public const string CONNECTION_STATUS_CONNECTION_IN_PROGRESS = "Connection in Progress";
        public const string CONNECTION_STATUS_DISCCONNECTION_IN_PROGRESS = "Disconnection in Progress";


        public const string CMW500_LTE_SIGNALING_CALL_PSWITCHED_ACTION = "CALL:LTE:SIGN:PSWitched:ACTion";

        public const string CMW500_LTE_SIGNALING_PACKETSWITCHED_STATE = "FETCh:LTE:SIGN:PSWitched:STATe";

        public const string CMW500_LTE_SIGNALING_UEREPORT_ENABLE = "CONFigure:LTE:SIGN:UEReport:ENABle";

        public const string CMW500_LTE_SIGNALING_UEREPORT_RSRP = "SENSe:LTE:SIGN:UEReport:RSRP";

        public const string CMW500_LTE_SIGNALING_UEREPORT_RSRQ = "SENSe:LTE:SIGN:UEReport:RSRQ";

        public const string CMW500_LTE_SIGNALING_UEREPORT_RSRP_RANGE = "SENSe:LTE:SIGN:UEReport:RSRP:RANGe";

        public const string CMW500_LTE_SIGNALING_UEREPORT_RSRQ_RANGE = "SENSe:LTE:SIGN:UEReport:RSRQ:RANGe";


        //TX Measurement
        //public const string CMW500_LTE_SIGNALING_ROUTE_MEASUREMENT = "ROUTe:LTE:MEAS1:SCENario:CSPath ""LTE Sig1"""

        public const string CMW500_LTE_SIGNALING_ABORT_MEASUEMENT_MULTIEVALUATION = "ABORT:LTE:MEAS:MEValuation";

        public const string CMW500_LTE_SIGNALING_INITIATE_MEASUEMENT_MULTIEVALUATION = "INITiate:LTE:MEAS:MEValuation";

        public const string CMW500_LTE_SIGNALING_FETCH_MEASUEMENT_MULTIEVALUATION_CURRENT = "FETCh:LTE:MEAS:MEValuation:MODulation:CURRent?";

        public const string CMW500_LTE_SIGNALING_FETCH_MEASUEMENT_MULTIEVALUATION_AVERAGE = "FETCh:LTE:MEAS:MEValuation:MODulation:AVERage?";

        public const string CMW500_LTE_MEAS_MEVALUATION_MSUBFRAMES = "CONFigure:LTE:MEAS:MEValuation:MSUBframes";

        public const string CMW500_LTE_SIGNALING_FETCH_MEASUEMENT_MEVALUATION_ACLR_CURRENT = "FETCH:LTE:MEAS:MEValuation:ACLR:CURRent?";

        public const string CMW500_LTE_SIGNALING_FETCH_MEASUEMENT_MEVALUATION_ACLR_AVERAGE = "FETCH:LTE:MEAS:MEValuation:ACLR:AVERage?";

        public const string CMW500_LTE_SIGNALING_FETCH_MEASUEMENT_MEVALUATION_SEMASK_CURRENT = "FETCH:LTE:MEAS:MEValuation:SEMask:CURRent?";

        public const string CMW500_LTE_SIGNALING_FETCH_MEASUEMENT_MEVALUATION_SEMASK_AVERAGE = "FETCH:LTE:MEAS:MEValuation:SEMask:AVERage?";



        // ===== Sensitivity (Extended BLER) =====  靈敏性

        public const string CMW500_LTE_SIGNALING_EBLER_SUBFRAMES = "CONFigure:LTE:SIGN:EBLer:SFRames";   //傳輸包的數量

        public const string CMW500_LTE_MEAS_MEVALUATION_REPETITION = "CONFigure:LTE:SIGN:EBLer:REPetition";  //重複方式設置

        public const string CMW500_LTE_SIGNALING_EBLER_STATE_RUN = "RUN";

        public const string CMW500_LTE_SIGNALING_EBLER_STATE_OFF = "OFF";

        public const string CMW500_LTE_SIGNALING_EBLER_STATE_READY = "RDY";

        public const string CMW500_LTE_SIGNALING_INIT_EBLER = "INIT:LTE:SIGN:EBLer";  //初始化BLER測試開關

        public const string CMW500_LTE_SIGNALING_ABORT_EBLER = "ABORT:LTE:SIGN:EBLer"; //終止BLER測試

        public const string CMW500_LTE_SIGNALING_FETCH_EBLER_ABSOLUTE = "FETCh:LTE:SIGN:EBLer:ABSolute";

        public const string CMW500_LTE_SIGNALING_FETCH_EBLER_RELATIVE = "FETCh:LTE:SIGN:EBLer:RELative";//

        public const string CMW500_LTE_SIGNALING_FETCH_EBLER_STATE = "FETCh:LTE:Sign:EBLer:State"; //檢測RX工作狀態

        //======== CMW270 Settings ============

        public const string CMW270_WLAN_SIGNALING_STANDARD = "CONF:WLAN:SIGN:CONN:STAN";
    }
}
