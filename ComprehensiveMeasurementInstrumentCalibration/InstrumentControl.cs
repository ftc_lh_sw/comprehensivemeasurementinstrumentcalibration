﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Ivi.Visa;
using Ivi.Visa.Interop;
using System.Runtime.InteropServices;
using System.Windows.Forms;


namespace ComprehensiveMeasurementInstrumentCalibration
{
    class InstrumentControl  //儀器控制
    {
        public string InterfaceWithAddress = "";
        ResourceManager ioMgr = new ResourceManager();
        FormattedIO488 instrument = new FormattedIO488();
        IVisaSession session = null;
        public static bool connectAbnormal = false;

        //連接到儀器
        public void InstrumentInitialize(string interfaceAddress)
        {
            try
            {
                InterfaceWithAddress = interfaceAddress;
                // , AccessMode.NO_LOCK, 3000, ""
                session = ioMgr.Open(InterfaceWithAddress);
            }
            catch (COMException ex)
            {
                MessageBox.Show("InstrumentInitialize Error:" + ex.Message);
            }
        }

        public void WriteVISACommand(string command)
        {
            try
            {
                instrument.IO = (IMessage)session;
                //instrument.IO.SendEndEnabled = false;
                //instrument.IO.Timeout = 1000;                       //in milliseconds
                //instrument.IO.TerminationCharacterEnabled = true;   //Defaults to false    
                instrument.WriteString(command);
                //instrument.IO.Close();
            }
            catch (Exception ex)
            {
                MessageBox.Show("WriteVISACommand(" + command + ") Error" + ex.Message);
            }
        }

        public string ReadVISACommandResult(string command)
        {
            string strReadVISACommandResult = "";
            try
            {
                instrument.IO = (IMessage)session;
                //instrument.IO.SendEndEnabled = false;
                //instrument.IO.Timeout = 1000;                       //in milliseconds
                //instrument.IO.TerminationCharacterEnabled = true;   //Defaults to false    
                instrument.WriteString(command);
                strReadVISACommandResult = instrument.ReadString();
                string[] arrTemp = (strReadVISACommandResult).Split(new[] { "\n" }, StringSplitOptions.None);
                strReadVISACommandResult = arrTemp[0];
                //instrument.IO.Close();

            }
            catch (COMException ex)
            {
                connectAbnormal = true;
                if (connectAbnormal == false)
                    MessageBox.Show("ReadVISACommandResult(" + command + ") Error" + ex.Message);
            }
            return strReadVISACommandResult;
        }






    }


}
