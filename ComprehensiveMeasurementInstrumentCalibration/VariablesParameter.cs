﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ComprehensiveMeasurementInstrumentCalibration
{
    class VariablesParameter
    {

        public static bool InstrumentConnected500;  //instrument connect bool value

        public static bool InstrumentConnectedN9030A;

        public static bool InstrumentConnected53230A;

        public static string INSTRUMENTID;   //instrument ID          

        public const string INSTRUMENTNOTFOUND = "連接儀器失敗！";   //instrument not found 


        public static string InterfaceAndAddressCMW500 = "";  //interface（接口） and address

        public static string InterfaceAndAddressN9030A = "";  //interface（接口） and address

        public static string InterfaceAndAddress53230A = ""; 


        public static int CMW270Packets = 1000;
        public static int CMW500Subframes = 200;
        public static double CMW270TXBurstPowerNum = -65;
        public static double CMW270TXBurstPowerNumLast = 0;
        //public static string SoftwareVersion;

        public static string CMW270ChannelStr = "";
        public static string CMW500ChannelStr = "";
        public static string[] CMW270ChannelArray = {};
        public static string[] CMW500ChannelArray = {};
        


        public static double CMW500RSEPRE = -85;

        public const string projectNAME = "LTE_WiFiCoexistTestTool";
        public static string LTEStateON = "LTE状态开";
        public static string WiFiStateON = "WiFi状态开";
        public static string stateOFF = "无信号干扰";
        public static bool LTEState = false;
        public static bool WiFiState = false;


        public static bool FileExit;
        //public static long SampleTime;
        //public static double InputFrequency;

        //public static double OutputVoltage;
        //public static double OutputCurrent;
        //public static double OutputPower;
        //public static double OutputFrequency;
        //public static string SoftwareVersion;
        //public static string CumulativeTime;



        public string  tryStandardStrToSCPIStr(string str)
        {
            string temp = "";
            switch (str)
            {
                case "IEEE 802.11b":
                    return temp = "BSTD";
                case "IEEE 802.11a":
                    return temp = "ASTD";
                case "IEEE 802.11g":
                    return temp = "GSTD";
                case "IEEE 802.11g (OFDM)":
                    return temp = "GOST";
                case "IEEE 802.11a/n":
                    return temp = "ANST";
                case "IEEE 802.11g/n":
                    return temp = "GNST";
                case "IEEE 802.11g (OFDM)/n":
                    return temp = "GONS";
                case "IEEE 802.11ac":
                    return temp = "ACST";
            }
            return temp;
        }

        public string trySCPIStrToStandardStr(string str)
        {
            string temp = "";
            switch (str)
            {
                case "BSTD":
                    return temp = "IEEE 802.11b";
                case "ASTD":
                    return temp = "IEEE 802.11a";
                case "GSTD":
                    return temp = "IEEE 802.11g";
                case "GOST":
                    return temp = "IEEE 802.11g (OFDM)";
                case "ANST":
                    return temp = "IEEE 802.11a/n";
                case "GNST":
                    return temp = "IEEE 802.11g/n";
                case "GONS":
                    return temp = "IEEE 802.11g (OFDM)/n";
                case "ACST":
                    return temp = "IEEE 802.11ac";
            }
            return temp;
        }



    }
}
